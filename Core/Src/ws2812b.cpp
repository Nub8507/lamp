/*
 * ws2812b.cpp
 *
 *  Created on: Aug 26, 2021
 *      Author: User
 */

#include <ws2812b.h>
#include <cstring>

namespace lamp_core {

ws2812b::ws2812b() {
	//BUF_DMA = new uint16_t[ARRAY_LEN];
}

void ws2812b::ws2812_init(void) {

	auto led_num = DELAY_LEN + LED_COUNT * 24;
	for (uint16_t i = DELAY_LEN - 1; i < led_num; ++i) {
		BUF_DMA[i] = HIGH;
	}
	for (uint16_t i = ARRAY_LEN - 4; i < ARRAY_LEN; ++i){
		BUF_DMA[i] = FULL;
	}
}

void ws2812b::ws2812_pixel_rgb_to_buf(uint8_t Rpixel, uint8_t Gpixel,
		uint8_t Bpixel, uint16_t posX) {
	//
	BUF_LED[posX].red=Rpixel;
	BUF_LED[posX].green=Gpixel;
	BUF_LED[posX].blue=Bpixel;
	//
	ws2812_pixel_rgb_to_buf_dma(posX);
	//
}

void ws2812b::ws2812_pixel_rgb_to_buf_dma(uint16_t posX) {

	//
	uint8_t Rpixel=BUF_LED[posX].red*light/max_light;
	uint8_t Gpixel=BUF_LED[posX].green*light/max_light;
	uint8_t Bpixel=BUF_LED[posX].blue*light/max_light;
	//
	if(Rpixel>MAX_LIGHT)Rpixel=MAX_LIGHT;
	if(Gpixel>MAX_LIGHT)Gpixel=MAX_LIGHT;
	if(Bpixel>MAX_LIGHT)Bpixel=MAX_LIGHT;
	//
	for (uint16_t i = 0; i < 8; i++) {
		auto bit_red = DELAY_LEN - 1 + posX * 24 + i + 8;
		if (BitIsSet(255 - Rpixel, (7 - i)) == 1) {
			BUF_DMA[bit_red] = HIGH;
		} else {
			BUF_DMA[bit_red] = LOW;
		}
		auto bit_green = DELAY_LEN - 1 + posX * 24 + i + 0;
		if (BitIsSet(255 - Gpixel, (7 - i)) == 1) {
			BUF_DMA[bit_green] = HIGH;
		} else {
			BUF_DMA[bit_green] = LOW;
		}

		auto bit_blue = DELAY_LEN - 1 + posX * 24 + i + 16;
		if (BitIsSet(255 - Bpixel, (7 - i)) == 1) {
			BUF_DMA[bit_blue] = HIGH;
		} else {
			BUF_DMA[bit_blue] = LOW;
		}
	}
}

void ws2812b::ws2812_rot_left() {
	//
	RGB t;
	t=BUF_LED[0];
	for(uint16_t posX =0;posX<LED_COUNT-1;++posX){
		BUF_LED[posX]=BUF_LED[posX+1];
		ws2812_pixel_rgb_to_buf_dma(posX);
	}
	BUF_LED[LED_COUNT - 1]=t;
	ws2812_pixel_rgb_to_buf_dma(LED_COUNT - 1);
	//
}

void ws2812b::change_light(int32_t light,int32_t max_light) {
	//
	this->light=light;
	this->max_light=max_light;
	//
	for(uint16_t i =0;i<LED_COUNT;++i){
		ws2812_pixel_rgb_to_buf_dma(i);
	}
}

void ws2812b::ws2812_pixel_hsl_to_buf(HSV pixel, uint16_t posX){
	//
	ws2812b::RGB rgb(0,0,0);
    //
    int Vmin=(100-pixel.S)*pixel.V / 100;
    int H = (pixel.H / 60)%6;
    int a=(pixel.V-Vmin)*(pixel.H%60)/60;
    int Vinc=Vmin+a;
    int Vdec=pixel.V-a;

    switch (H) {
    case 0:
        rgb.red=pixel.V;
        rgb.green=Vinc;
        rgb.blue=Vmin;
        break;
    case 1:
        rgb.red=Vdec;
        rgb.green=pixel.V;
        rgb.blue=Vmin;
        break;
    case 2:
        rgb.red=Vmin;
        rgb.green=pixel.V;
        rgb.blue=Vinc;
        break;
    case 3:
        rgb.red=Vmin;
        rgb.green=Vdec;
        rgb.blue=pixel.V;
        break;
    case 4:
        rgb.red=Vinc;
        rgb.green=Vmin;
        rgb.blue=pixel.V;
        break;
    case 5:
        rgb.red=pixel.V;
        rgb.green=Vmin;
        rgb.blue=Vdec;
        break;
    default:
        break;
    }
    //
    ws2812_pixel_rgb_to_buf(rgb.red,rgb.green,rgb.blue,posX);
}

} /* namespace lamp_core */
