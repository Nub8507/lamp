/*
 * logic.cpp
 *
 *  Created on: Aug 26, 2021
 *      Author: User
 */

#include <logic.h>
#include <sys/_stdint.h>
#include <cstdlib>
#include "main.h"

namespace lamp_core {

logic::logic(int_struct &s_int, devices &dev_box, uint16_t led_pin) :
		dev(dev_box), interrupts(s_int), led_pin_num(led_pin) {

	data_driver.ws2812_init();

	set_rgb();

}

void logic::check_interrupts() {

	//
	if (interrupts.main_btn) {
		main_btn_press();
	}
	//
	if (interrupts.timer2_fin) {
		interrupts.timer2_fin = 0;
		if (repeat_num > 0) {
			repeat_num--;
			start_send();
		} else {
			HAL_TIM_PWM_Stop_DMA(dev.getHtim2(), TIM_CHANNEL_2);
			repeat_num = 2;
		}
	}
	//
	if (interrupts.adc1_fin) {
		interrupts.adc1_fin = 0;
		light_check();
	}
	//
	if (interrupts.sensor_btn) {
		interrupts.sensor_btn = 0;
		sensor_btn_press();
	}
	//
	if (interrupts.timer1_fin) {
		interrupts.timer1_fin = 0;
		rotate();
	}
	//

}

void logic::set_rgb() {
	//
	for (int i = 0; i < data_driver.LED_COUNT; i += 3) {
		data_driver.ws2812_pixel_rgb_to_buf(0, 128, 0, i);
	}
	for (int i = 1; i < data_driver.LED_COUNT; i += 3) {
		data_driver.ws2812_pixel_rgb_to_buf(128, 0, 0, i);
	}
	for (int i = 2; i < data_driver.LED_COUNT; i += 3) {
		data_driver.ws2812_pixel_rgb_to_buf(0, 0, 128, i);
	}
	//
}

void logic::set_off() {
	for (int i = 0; i < data_driver.LED_COUNT; i++) {
		data_driver.ws2812_pixel_rgb_to_buf(0, 0, 0, i);
	}
}

void logic::set_red() {
	for (int i = 0; i < data_driver.LED_COUNT; i++) {
		data_driver.ws2812_pixel_rgb_to_buf(128, 0, 0, i);
	}
}

void logic::set_green() {
	for (int i = 0; i < data_driver.LED_COUNT; i++) {
		data_driver.ws2812_pixel_rgb_to_buf(0, 128, 0, i);
	}
}

void logic::set_blue() {
	for (int i = 0; i < data_driver.LED_COUNT; i++) {
		data_driver.ws2812_pixel_rgb_to_buf(0, 0, 128, i);
	}
}

void logic::set_white() {
	for (int i = 0; i < data_driver.LED_COUNT; i++) {
		data_driver.ws2812_pixel_rgb_to_buf(128, 128, 128, i);
	}
}

void logic::start_send() {
	if (HAL_TIM_PWM_Start_DMA(dev.getHtim2(), TIM_CHANNEL_2,
			(uint32_t*) data_driver.getBufDma(), data_driver.ARRAY_LEN - 1)
			!= HAL_OK) {
		//NVIC_SystemReset();
	}
}

void logic::main_btn_press() {
	//
	interrupts.main_btn = 0;
	HAL_TIM_Base_Stop_IT(dev.getHtim3());
	//
	switch (work_status) {
	case WORK_MODE::MODE_OFF:
		work_status = WORK_MODE::MODE_AUTO;
		led_power_off();
		led_pin_num = blueOut_Pin;
		main_status = MAIN_STATUS::MAIN_STATUS_RGB;
		dop_status = DOP_STATUS::DOP_STATUS_OFF;
		//
		HAL_ADCEx_Calibration_Start(dev.getHadc1());
		HAL_ADC_Start_IT(dev.getHadc1());
		HAL_TIM_Base_Start(dev.getHtim3());
		break;
	case WORK_MODE::MODE_AUTO:
		//
		work_status = WORK_MODE::MODE_FULL;
		led_power_off();
		led_pin_num = greenOut_Pin;
		main_status = MAIN_STATUS::MAIN_STATUS_RGB;
		dop_status = DOP_STATUS::DOP_STATUS_OFF;
		//
		HAL_ADCEx_Calibration_Start(dev.getHadc1());
		HAL_ADC_Start_IT(dev.getHadc1());
		HAL_TIM_Base_Start(dev.getHtim3());
		//
		break;
	case WORK_MODE::MODE_FULL:
		work_status = WORK_MODE::MODE_OFF;
		led_power_off();
		led_pin_num = redOut_Pin;
		dop_status = DOP_STATUS::DOP_STATUS_OFF;
		HAL_ADC_Stop_IT(dev.getHadc1());
		HAL_TIM_Base_Stop(dev.getHtim3());
		break;
	}
	//
	set_light_from_mode();
	//
}

void logic::sensor_btn_press() {
	//
	auto_on_off = true;
	//
	interrupts.sensor_btn = 0;
	switch_main_status();
	set_light_from_mode();
	//
}

void logic::set_rainbow() {
	//
	const int step = 360 / data_driver.LED_COUNT;
	//
	for (int i = 0; i < data_driver.LED_COUNT; i++) {
		data_driver.ws2812_pixel_hsl_to_buf( { static_cast<uint16_t>(i * step),
				100, 40 }, i);
	}
	//
	start_send();
	//
}

void logic::light_check() {

	int32_t adc = HAL_ADC_GetValue(dev.getHadc1());
	//
	if (adc < 250)
		adc = 250;
	//
	curr_light = adc;
	//
	if (work_status == WORK_MODE::MODE_AUTO) {
		auto l_off = switch_light + 40;
		auto l_on = switch_light - 40;
		if (curr_light >= l_off && auto_on_off == true) {
			set_off();
			auto_on_off = false;
			last_dop_status = dop_status;
			dop_status = DOP_STATUS::DOP_STATUS_OFF;
			return;
		} else if (curr_light < l_on && auto_on_off == false) {
			dop_status = last_dop_status;
			set_light_from_mode();
			auto_on_off = true;
		}
	}
	//
	auto t1 = last_light - curr_light;
	if (t1 > last_light / 10 || t1 < -last_light / 10 || t1 * t1 > 6400) {
		data_driver.change_light(curr_light, max_light);
		last_light = curr_light;
		start_send();
	}
}

void logic::tick() {

	static uint32_t last_switch;
	static bool t1;
	static uint32_t delta = 10;
	//
	auto tic_val = HAL_GetTick();
	if (tic_val - last_switch > delta) {
		if (t1) {
			last_switch = tic_val;
			delta = 1;
		} else {
			last_switch = tic_val;
			delta = 12;
		}
		HAL_GPIO_WritePin(GPIOA, led_pin_num, (GPIO_PinState) t1);
		t1 = !t1;
	}
	//
}

void logic::rotate() {
	data_driver.ws2812_rot_left();
	start_send();
}

void logic::set_light_from_mode() {
	//
	if (work_status == WORK_MODE::MODE_OFF) {
		set_off();
		start_send();
		return;
	}
	//
	switch (main_status) {
	case MAIN_STATUS::MAIN_STATUS_RGB:
		HAL_TIM_Base_Stop(dev.getHtim3());
		set_rgb();
		HAL_TIM_Base_Start(dev.getHtim3());
		break;
	case MAIN_STATUS::MAIN_STATUS_RAINBOW:
		HAL_TIM_Base_Stop(dev.getHtim3());
		set_rainbow();
		HAL_TIM_Base_Start(dev.getHtim3());
		break;
	case MAIN_STATUS::MAIN_STATUS_WHITE:
		HAL_TIM_Base_Stop(dev.getHtim3());
		set_white();
		HAL_TIM_Base_Start(dev.getHtim3());
		break;
	}
	//
	switch (dop_status) {
	case DOP_STATUS::DOP_STATUS_OFF:
		HAL_TIM_Base_Stop_IT(dev.getHtim1());
		break;
	case DOP_STATUS::DOP_STATUS_SLOW:
		dev.getHtim1()->Init.Prescaler = 1000;
		HAL_TIM_Base_Init(dev.getHtim1());
		HAL_TIM_Base_Start_IT(dev.getHtim1());
		break;
	case DOP_STATUS::DOP_STATUS_MIDIUM:
		dev.getHtim1()->Init.Prescaler = 200;
		HAL_TIM_Base_Init(dev.getHtim1());
		HAL_TIM_Base_Start_IT(dev.getHtim1());
		break;
	case DOP_STATUS::DOP_STATUS_FAST:
		dev.getHtim1()->Init.Prescaler = 50;
		HAL_TIM_Base_Init(dev.getHtim1());
		HAL_TIM_Base_Start_IT(dev.getHtim1());
		break;
	}
	//
	start_send();
	//
}

void logic::switch_main_status() {
	switch (main_status) {
	case MAIN_STATUS::MAIN_STATUS_RGB:
		if (switch_dop_status()) {
			last_light = 6050;
			main_status = MAIN_STATUS::MAIN_STATUS_RAINBOW;
			dop_status = DOP_STATUS::DOP_STATUS_OFF;
		}
		break;
	case MAIN_STATUS::MAIN_STATUS_RAINBOW:
		if (switch_dop_status()) {
			last_light = 6050;
			main_status = MAIN_STATUS::MAIN_STATUS_WHITE;
			dop_status = DOP_STATUS::DOP_STATUS_OFF;
		}
		break;
	case MAIN_STATUS::MAIN_STATUS_WHITE:
		if (switch_dop_status()) {
			last_light = 6050;
			main_status = MAIN_STATUS::MAIN_STATUS_RGB;
			dop_status = DOP_STATUS::DOP_STATUS_OFF;
		}
		break;
	}
}

bool logic::switch_dop_status() {
	//
	switch (dop_status) {
	case DOP_STATUS::DOP_STATUS_OFF:
		if (main_status == MAIN_STATUS::MAIN_STATUS_RGB
				|| main_status == MAIN_STATUS::MAIN_STATUS_RAINBOW) {
			dop_status = DOP_STATUS::DOP_STATUS_SLOW;
			return false;
		}
		return true;
	case DOP_STATUS::DOP_STATUS_SLOW:
		dop_status = DOP_STATUS::DOP_STATUS_MIDIUM;
		return false;
	case DOP_STATUS::DOP_STATUS_MIDIUM:
		dop_status = DOP_STATUS::DOP_STATUS_FAST;
		return false;
	case DOP_STATUS::DOP_STATUS_FAST:
		dop_status = DOP_STATUS::DOP_STATUS_OFF;
		return true;
	}
	//
}

void logic::led_power_off() {
	HAL_GPIO_WritePin(greenOut_GPIO_Port, greenOut_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(blueOut_GPIO_Port, blueOut_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(redOut_GPIO_Port, redOut_Pin, GPIO_PIN_RESET);
}

} /* namespace lamp_core */
