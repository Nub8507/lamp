/*
 * ws2812b.h
 *
 *  Created on: Aug 26, 2021
 *      Author: User
 */

#ifndef WS2812B_H_
#define WS2812B_H_

#include <sys/_stdint.h>

namespace lamp_core {

class ws2812b {
public:
	ws2812b();

	static const uint16_t LED_COUNT = 94;
	static const uint16_t DELAY_LEN = 70;
	static const uint16_t ARRAY_LEN = ((DELAY_LEN + LED_COUNT*24)+3);

	static const uint16_t HIGH = 35;			//42
	static const uint16_t LOW = 63;				//82
	static const uint16_t FULL = 89;

	static const uint8_t MAX_LIGHT = 100;

	struct HSV{
		uint16_t H;
		uint16_t S;
		uint16_t V;

		HSV():
			H(0),S(0),V(0){}

		HSV(uint16_t h,uint16_t s, uint16_t v):
			H(h),S(s),V(v){}

	};


	void ws2812_init(void);
	void ws2812_pixel_rgb_to_buf(uint8_t Rpixel, uint8_t Gpixel, uint8_t Bpixel,
			uint16_t posX);
	void ws2812_rot_left();
	void change_light(int32_t light,int32_t max_light);
	void ws2812_pixel_hsl_to_buf(HSV pixel,	uint16_t posX);


	const uint16_t* getBufDma() const {
		return BUF_DMA;
	}

private:

	struct RGB{
		uint8_t red;
		uint8_t green;
		uint8_t blue;

		RGB():
			red(0),green(0),blue(0){}

		RGB(uint8_t r,uint8_t b, uint8_t g):
			red(r),green(g),blue(b){}


	};

	uint16_t BUF_DMA[ARRAY_LEN] = {0};
	RGB BUF_LED[LED_COUNT] = {{0,0,0}};
	RGB BACK_BUF_LED[LED_COUNT] = {{0,0,0}};
	int32_t light=1;
	int32_t max_light=1;


	inline bool BitIsSet(uint16_t reg, uint16_t bit) {
		return ((reg & (1 << bit)) != 0);
	}

	void ws2812_pixel_rgb_to_buf_dma(uint16_t posX);

};

} /* namespace lamp_core */

#endif /* WS2812B_H_ */
