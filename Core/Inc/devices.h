/*
 * devices.h
 *
 *  Created on: 29 авг. 2021 г.
 *      Author: User
 */

#ifndef INC_DEVICES_H_
#define INC_DEVICES_H_

#include <stm32f1xx.h>

namespace lamp_core {

class devices {
public:
	devices();

	inline ADC_HandleTypeDef* getHadc1()
				{return hadc1;}
	void setHadc1(ADC_HandleTypeDef *hadc)
				{this->hadc1 = hadc;}

	inline DMA_HandleTypeDef* getHdmaTim2Ch2Ch4()
				{return hdma_tim2_ch2_ch4;}
	void setHdmaTim2Ch2Ch4(DMA_HandleTypeDef *hdmaTim2Ch2Ch4)
				{hdma_tim2_ch2_ch4 = hdmaTim2Ch2Ch4;}
	inline TIM_HandleTypeDef* getHtim1()
				{return htim1;}
	void setHtim1(TIM_HandleTypeDef *htim1)
				{this->htim1 = htim1;}
	inline TIM_HandleTypeDef* getHtim2()
				{return htim2;}
	void setHtim2(TIM_HandleTypeDef *htim2)
				{this->htim2 = htim2;}
	inline TIM_HandleTypeDef* getHtim3()
				{return htim3;}
	void setHtim3(TIM_HandleTypeDef *htim3)
				{this->htim3 = htim3;}

private:

	ADC_HandleTypeDef *hadc1;

	TIM_HandleTypeDef *htim1;
	TIM_HandleTypeDef *htim2;
	TIM_HandleTypeDef *htim3;
	DMA_HandleTypeDef *hdma_tim2_ch2_ch4;

};

} /* namespace lamp_core */

#endif /* INC_DEVICES_H_ */
