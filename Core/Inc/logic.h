/*
 * logic.h
 *
 *  Created on: Aug 26, 2021
 *      Author: User
 */

#ifndef LOGIC_H_
#define LOGIC_H_

#include <ws2812b.h>
#include "int_struct.h"
#include "devices.h"
#include <stm32f1xx.h>

namespace lamp_core {

template <class p,size_t size>
class integr{
public:

	integr(){
		b_start=0;
		b_size=0;
	}

	integr(const p& val){
		b_start=0;
		b_size=1;
		buffer[0]=val;
	}

	operator p(){
		if(b_size==0)
			return p();
		//
		p rez=buffer[b_start];
		auto pos=b_start;
		for(auto i=1;i<b_size;++i){
			pos+=i;
			if(pos>b_size-1)pos=0;
			rez+=buffer[pos];
		}
		return rez/b_size;
	}

	integr<p,size>& operator=(const p& val){
		auto pos=b_start+b_size;
		if(pos>=size){
			pos=pos-size;
		}
		buffer[pos]=val;
		b_size++;
		if(b_size>size){
			b_size=size;
			b_start++;
			if(b_start>=size){
				b_start-=size;
			}
		}
		return *this;
	}
private:
	p buffer[size];
	size_t b_start;
	size_t b_size;
};

class logic {
public:
	logic(int_struct &s_int, devices &dev_box, uint16_t led_pin);

	void check_interrupts();
	void tick();

private:

	enum class WORK_MODE {
		MODE_OFF,
		MODE_AUTO,
		MODE_FULL,
	};

	enum class MAIN_STATUS {
		MAIN_STATUS_RGB,
		MAIN_STATUS_RAINBOW,
		MAIN_STATUS_WHITE,
	};

	enum class DOP_STATUS {
		DOP_STATUS_OFF,
		DOP_STATUS_SLOW,
		DOP_STATUS_MIDIUM,
		DOP_STATUS_FAST,
	};

	lamp_core::ws2812b data_driver;
	lamp_core::devices &dev;
	int_struct &interrupts;

	WORK_MODE work_status = WORK_MODE::MODE_OFF;
	MAIN_STATUS main_status = MAIN_STATUS::MAIN_STATUS_RGB;
	DOP_STATUS dop_status = DOP_STATUS::DOP_STATUS_OFF;
	uint8_t repeat_num = 2;
	bool auto_on_off = true;
	DOP_STATUS last_dop_status = DOP_STATUS::DOP_STATUS_OFF;
	uint16_t led_pin_num = 0;

	const int32_t max_light = 4050;
	const int32_t switch_light =480;
	int32_t last_light = 4050;
	integr<int32_t,6> curr_light = 4050;

	void set_light_from_mode();
	void switch_main_status();
	bool switch_dop_status();

	void set_rgb();
	void set_off();
	void set_red();
	void set_green();
	void set_blue();
	void set_rainbow();
	void set_white();
	void start_send();
	void main_btn_press();
	void sensor_btn_press();
	void light_check();
	void rotate();
	void led_power_off();

};

} /* namespace lamp_core */

#endif /* LOGIC_H_ */
